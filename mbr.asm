BITS 16
jmp short bootloader_start
nop

SectorsPerTrack		dw 0		; Sectors per track 
Sides			dw 0		; Number of sides/heads
Signature		db 0		; Drive signature

bootloader_start:
    mov ax, 07C0h	; Set up 4K of stack space above buffer
	add ax, 544		; 8k buffer = 512 paragraphs + 32 paragraphs (loader)
	cli				; Disable interrupts while changing stack
	mov ss, ax
	mov sp, 4096
	sti				; Restore interrupts

    mov ax, 07C0h	; Set data segment to where we're loaded
	mov ds, ax

    mov [bootdev], dl

    mov ah, 8   ; Get drive parameters
	int 13h
    jc disk_error
    and cx, 3Fh
    mov [SectorsPerTrack], cx
    movzx dx, dh		; Maximum head number
	add dx, 1			; Head numbers start at 0 - add 1 for total
	mov [Sides], dx
read_program:
    mov ax, 2000h ; Segment where we'll load the program (0x2000:0x0000)
    mov es, ax
    mov bx, 0

    mov al, byte [program_size] ; How big the program is
    mov cx, 1 ; Start after mbr which is in sector 0

    call read_sectors

    push ax
    mov al, byte [es:bx] ; This byte is used to show the bootloader that the program exists on disk.
    cmp al, 1
    jne no_program
    pop ax

    cli				; Clear interrupts
	mov ax, 0
	mov ss, ax		; Set stack segment and pointer
	mov sp, 0FFFFh
	sti				; Restore interrupts
	cld
    mov ax, 2000h
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

    mov ah, 0
    mov al, 0002h
    int 10h ; Set video mode

    mov ah, 86h
    mov cx, 0010
    mov dx, 0000
    int 15h

    mov si, [1] ; Position of text1_start
    mov bl, 2
    mov cx, 0000 ; How long the print function should wait before printing the next character (CX:DX)
    mov dx, 0150
    call print_string

    mov si, [3] ; Position of text2_start (WORD = 2 bytes)
    mov cx, 0001
    mov dx, 9999
    call print_string

    jmp $ ; Program done. Jump here aka infinite loop

; Reads al sectors from cx to memory at es:bx
read_sectors:
    pusha
    mov dh, 6 ; Five tries before error
.LOOP:
    dec dh
    push ax
    mov ax, cx
    call l2hts
    pop ax
    mov ah, 2
    int 0x13
    jnc .DONE
    call reset_disk
    jc disk_error
    cmp dh, 0
    jne .LOOP
    mov si, disk_read_error
    call print_string
    jmp reboot
.DONE:
    popa
    ret
reset_disk:
    push ax
    push dx
    mov ax, 0
    mov dl, byte [bootdev]
    stc
    int 0x13
    pop dx
    pop ax
    ret
disk_error:
    mov si, fdisk_error
    call print_string ; We don't need to jump to reboot since it's right under us ;)
reboot:
    mov ax, 0
    int 16h
    mov ax, 0
    int 19h
print_string:
    pusha
    mov ah, 0Eh
.REPEAT:
    lodsb
    cmp al, 0
    je .DONE
    int 10h

    push ax ; BIOS wait
    mov ah, 86h
    int 15h
    pop ax

    jmp short .REPEAT
.DONE:
    popa
    ret
l2hts:			; Calculate head, track and sector settings for int 13h		; IN: logical sector in AX, OUT: correct registers for int 13h
	push bx
	push ax

	mov bx, ax			; Save logical sector

	mov dx, 0			; First the sector
	div word [SectorsPerTrack]
	add dl, 01h			; Physical sectors start at 1
	mov cl, dl			; Sectors belong in CL for int 13h
	mov ax, bx

	mov dx, 0			; Now calculate the head
	div word [SectorsPerTrack]
	mov dx, 0
	div word [Sides]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]		; Set correct device

	ret
no_program:
    mov si, no_program_msg
    call print_string
    jmp reboot

bootdev db 0
program_size db 3 ; Program size in sectors

disk_read_error db 'Could not read disk!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0
fdisk_error db 'Fatal disk error!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0
no_program_msg db 'No program found!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0

times 510-($-$$) db 0	; Pad remainder of boot sector with zeros
dw 0AA55h		; Boot signature (DO NOT CHANGE!)
