BITS 16
TIMES 512 DB 0 ; I dont know how to do offsets in dd so i need to write this program to disk first before the bootloader and this is my solution ¯\_(ツ)_/¯.
;jmp short start

db 1 ; Here to show the bootloader that the program exists 

; Start variables
text1_start dw 52 + 1 + 4 ; Size of first text (including the numbers) + security byte + string start properties
text2_start dw 1 + 4 ; Security byte + string start properties

text2 db 'I hope this made your Monday a little bit better :)', 0

text1 db ' _____       _________ ______ ______ _   _          _____ ______ _____   _____ ', 0x0D, 0x0A ; String continues intil NULL
      db '|  __ \     / /__   __|  ____|  ____| \ | |   /\   / ____|  ____|  __ \ / ____|', 0x0D, 0x0A
      db '| |__) |   / /   | |  | |__  | |__  |  \| |  /  \ | |  __| |__  | |__) | (___  ', 0x0D, 0x0A ; 0x0D, 0x0A = Move one line down and move all the way to left
      db '|  _  /   / /    | |  |  __| |  __| | . ` | / /\ \| | |_ |  __| |  _  / \___ \ ', 0x0D, 0x0A
      db '| | \ \  / /     | |  | |____| |____| |\  |/ ____ \ |__| | |____| | \ \ ____) |', 0x0D, 0x0A
      db '|_|  \_\/_/      |_|  |______|______|_| \_/_/    \_\_____|______|_|  \_\_____/ ', 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A, 0x0D, 0x0A
      db 0x00 ; String ends here

times (512*4)-($-$$) db 0 ; Make the program 3 sectors big (ignoring the first sector)