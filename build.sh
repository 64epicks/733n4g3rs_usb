# WARNING: THIS WILL DESTROY ALL DATA ON THE DEVICE
dev="disk.flp"

# Clean device aka format
dd status=noxfer conv=notrunc if=/dev/zero of=$dev bs=512 count=4

# Build the assembly
nasm -f bin -o mbr.bin mbr.asm
nasm -f bin -o program.bin program.asm

# Copy to the device. Remember to write the program first otherwise it wont work
dd status=noxfer conv=notrunc if=program.bin of=$dev
dd status=noxfer conv=notrunc if=mbr.bin of=$dev
