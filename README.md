# Hello there, r/teenagers
So you want to run my code? Here you go:
## Linux
You'll need the program NASM (you should be able to install it with your package manager)
* Insert the USB you want to use (remember that this will erase all data on it).
* Get the device name for the USB (should be somthing like '/dev/[SOMETHING]').
* Edit the build.sh script.
* Change the variable 'dev="disk.flp"' into 'dev="[YOUR DEVICE]'. Then save.
* Run 'sh build.sh'

## Windows
Not currently available

## Booting
* Insert the USB when the computer is switched off.
* Start the computer
* Press F8 or F12 depending on your bios until the boot menu pops upp
* Select your usb and press enter.
* Done!

## Composing your own message
All the text should be written in program.asm. You can have a maximum of 1531 characters. NASM will send you an error if you write longer than that.
### Suffixes
I expect that you have looked at program.asm by now. And you have probaly figured out how it works. But if you do not know what `0x0D, 0x0A` and `0x00` does. Here is it:
> `0x0D, 0x0A` = New line
>
> `0x00` = End of string
### Start variables
The print function needs the memory addresses of the strings to be able to print. So you need to set the start variables. `text2_start` should be the same, as long as you don't write anything over it. The value of `text1_start` should be the length of `text2` (each suffix counts as one) plus 5 (in the document it's 1 + 4).

REMEMER THE NULL BYTE